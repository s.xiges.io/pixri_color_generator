package model


/*type Theme struct {
	PrimaryColor     string `json:"primary_color"`
	SecondaryColor   string `json:"secondary_color"`
	PrimaryDarkColor string `json:"primary_dark_color"`
	BodyColor string `json:"body_color"`
	TextColorBody string `json:"text_color_body"`
	TextColorAppBar string `json:"text_color_appBar"`
	Application   Application `gorm:"foreignkey:application_id" json:"application"`
	ApplicationID int `json:"application_id"`
}*/



type Theme struct {
	PrimaryColor     string `json:"primary_color"`
	SecondaryColor   string `json:"secondary_color"`
	TertiaryColor string `json:"tertiary_color"`
	PrimaryLightColor    string `json:"primary_light_color"`
	PrimaryMediumColor   string `json:"primary_medium_color"`
	PrimaryDarkColor     string `json:"primary_dark_color"`
	SecondaryLightColor    string `json:"secondary_light_color"`
	SecondaryMediumColor   string `json:"secondary_medium_color"`
	SecondaryDarkColor     string `json:"secondary_dark_color"`
	TertiaryLightColor string `json:"tertiary_light_color"`
	TertiaryMediumColor string `json:"tertiary_medium_color"`
	TertiaryDarkColor string `json:"tertiary_dark_color"`
	PrimaryTextColor     string `json:"primary_text_color"`
	PrimaryLightTextColor    string `json:"primary_light_text_color"`
	PrimaryMediumTextColor   string `json:"primary_medium_text_color"`
	PrimaryDarkTextColor     string `json:"primary_dark_text_color"`
	SecondaryTextColor   string `json:"secondary_text_color"`
	SecondaryLightTextColor    string `json:"secondary_light_text_color"`
	SecondaryMediumTextColor   string `json:"secondary_medium_text_color"`
	SecondaryDarkTextColor     string `json:"secondary_dark_text_color"`
	TertiaryTextColor string `json:"tertiary_text_color"`
	TertiaryLightTextColor string `json:"tertiary_light_text_color"`
	TertiaryMediumTextColor string `json:"tertiary_medium_text_color"`
	TertiaryDarkTextColor string `json:"tertiary_dark_text_color"`
	Application   Application `json:"application"`
	ApplicationID int `json:"application_id"`
}