package main

import (
	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	_ "github.com/lucasb-eyer/go-colorful"
	_ "image"
	_ "image/draw"
	_ "pixri_resource_handler/pkg"
	"pixri_resource_handler/pkg/controller"
	_ "pixri_resource_handler/resources"
)

func main(){

	e := echo.New()
	e.Use(middleware.Logger())
	e.Use(middleware.Recover())
	e.Use(middleware.CORS())

	r := e.Group("/")
	controller.ThemeController(r, "api")
	e.Logger.Fatal(e.Start(":5002"))

}