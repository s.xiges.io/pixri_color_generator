package controller

import (
	"github.com/labstack/echo/v4"
	"net/http"
	"pixri_resource_handler/pkg/generator"
	"pixri_resource_handler/pkg/model"
)

func GenerateTheme(c echo.Context) error {
	application := model.Application{}
	if error := c.Bind(&application); error != nil {
		return error
	}

	theme := generator.GenerateWebTheme(application.Name,application.Description,application.ID)

	return c.JSON(http.StatusOK, theme)
}


func ThemeController(g *echo.Group, contextRoot string) {
	g.POST(contextRoot+"/themes/generate", GenerateTheme)
}